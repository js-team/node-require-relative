# Installation
> `npm install --save @types/require-relative`

# Summary
This package contains type definitions for require-relative (https://github.com/kamicane/require-relative).

# Details
Files were exported from https://www.github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/require-relative

Additional Details
 * Last updated: Wed, 17 Jan 2018 22:17:28 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by Mattias Buelens <https://github.com/MattiasBuelens>.
